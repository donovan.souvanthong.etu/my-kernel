#include "gdt.h"
#include "idt.h"
#include "ioport.h"

#define NULL 0

typedef void (func_t)(void *);

enum STATUS {READY, ACTIVABLE, TERMINATED};	
 
struct ctx_s {
    void * esp;
    void * ebp;

    func_t * func;
    void * args;
 
    void * stack;

	enum STATUS status;
    struct ctx_s * next;
};

struct sem_s{
  int val;
  struct ctx_s * ctx;
};


int init_ctx(struct ctx_s *ctx, int stack_size, func_t f, void *args); // exo6
void start_ctx(struct ctx_s * ctx);
void switch_to_ctx(struct ctx_s *ctx); //exo7
int create_ctx(struct ctx_s *ctx, int stack_size, func_t f, void *args); //exo8
void yield(); //exo9
void * my_malloc(int stack);

void init_sem(struct sem_s *sem, int val);
void sem_up(struct sem_s *sem);
void sem_down(struct sem_s *sem);


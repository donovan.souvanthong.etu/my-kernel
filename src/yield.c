#include "yield.h"

char * tab_malloc[100000]; 
void * tab_malloc_ptr = tab_malloc;

struct ctx_s *ctx_courant;
static struct ctx_s *cercle;

int init_ctx(struct ctx_s *ctx, int stack_size, func_t f, void *args) {
    (ctx->func) = f;
    (ctx->args) = args;
    (ctx->stack) = my_malloc(stack_size);
    (ctx->esp) = (ctx->stack);
    (ctx->ebp) = (ctx->stack);
	(ctx->status) = READY;   
    return 0;
}

void start_ctx(struct ctx_s * ctx) {
    (ctx_courant->status) = ACTIVABLE;
    (ctx_courant->func)(ctx_courant->args);
    (ctx_courant->status) = TERMINATED;
    yield();
}

void switch_to_ctx(struct ctx_s *ctx) {
    if(ctx_courant!=NULL) {
        //Permet de sauvegarder le contexte
        //mov %%rsp, %0, transfers the contents of rsp to %0.
        asm("mov %%esp, %0"
        : "=r" (ctx_courant -> esp));
        
        asm("mov %%ebp, %0"
        : "=r" (ctx_courant -> ebp));
    }
    
    ctx_courant = ctx;
    //Permet de restaurer le contexte sauvegardé 
    //printf("RESTAURATION\n"); 
    asm("mov %0, %%esp"
    :
    : "r" (ctx_courant -> esp));

    asm("mov %0, %%ebp"
    :
    : "r" (ctx_courant -> ebp));

    if((ctx_courant->status) == READY) {
        start_ctx(ctx);
    } 


    return;
};

int create_ctx(struct ctx_s *ctx, int stack_size, func_t f, void *args){
	init_ctx(ctx, stack_size, f, args);
    
    __asm volatile("cli"); 

    if(cercle){
        cercle->next = ctx;
        ctx->next = cercle->next;
    } else {
        cercle->next = ctx;
        cercle = ctx;
    }    
    __asm volatile("sti"); 
    _outb(0xA0, 0x20); 
    _outb(0x20, 0x20);

    return 0;    
}

void yield(){
    if (ctx_courant == NULL){
	    switch_to_ctx(ctx_courant->next);
    }
}

void * my_malloc(int stack_size) {
    void * ptr = tab_malloc_ptr;
    tab_malloc_ptr+= stack_size;
    return tab_malloc_ptr;
}


void init_sem(struct sem_s *sem, int val) {
    __asm volatile("cli");
    sem->val = val;
    sem->ctx = NULL;
    __asm volatile("sti");
}

void sem_up(struct sem_s *sem) {
    /* TODO */
}

void sem_down(struct sem_s *sem) {
    /* TODO */
}

#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "yield.h"

struct ctx_s ctx_ping;
struct ctx_s ctx_pong;

void clear_screen();				/* clear screen */
void putc(char aChar);				/* print a single char on screen */
void puts(char *aString);			/* print a string on the screen */
void puthex(int aNumber);			/* print an Hex number on screen */
void controller();
char keyboard_map(unsigned char input);
void funct_time();
char* itoa(int num, int base);

void empty_irq(int_regs_t *r) {
}

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{
    /* clear the screen */
    clear_screen();
    puts("Early boot.\n"); 
    puts("\t-> Setting up the GDT... ");
    gdt_init_default();
    puts("done\n");

    puts("\t-> Setting up the IDT... ");
    setup_idt();
    puts("OK\n");
	
    puts("\n\n");

    puts("hello world\n");
 
    // idt_setup_handler(0, empty_irq); 
    // idt_setup_handler(0, empty_irq); 

    create_ctx(&ctx_ping, 100000, funct_time, NULL);
    create_ctx(&ctx_pong, 100000, controller, NULL);

    __asm volatile("cli");

    //idt_setup_handler(0, empty_irq); 
    idt_setup_handler(0, yield); 
 	  idt_setup_handler(1, controller);

    __asm volatile("sti");

    /* minimal setup done ! */

    // controller(); // Question 1.3
    
    for(;;) ; /* nothing more to do... really nothing ! */
}

/* base address for the video output assume to be set as character oriented by the multiboot */
unsigned char *video_memory = (unsigned char *) 0xB8000;

/* clear screen */
void clear_screen() {
  int i;
  for(i=0;i<80*25;i++) { 			/* for each one of the 80 char by 25 lines */
    video_memory[i*2+1]=0x0F;			/* color is set to black background and white char */
    video_memory[i*2]=(unsigned char)' '; 	/* character shown is the space char */
  }
}

/* print a string on the screen */
void puts(char *aString) {
  char *current_char=aString;
  while(*current_char!=0) {
    putc(*current_char++);
  }
}

/* print an number in hexa */
char *hex_digit="0123456789ABCDEF";
void puthex(int aNumber) {
  int i;
  int started=0;
  for(i=28;i>=0;i-=4) {
    int k=(aNumber>>i)&0xF;
    if(k!=0 || started) {
      putc(hex_digit[k]);
      started=1;
    }
  }
}

/* print a char on the screen */
int cursor_x=0;					/* here is the cursor position on X [0..79] */
int cursor_y=0;					/* here is the cursor position on Y [0..24] */

void setCursor() {
  int cursor_offset = cursor_x+cursor_y*80;
  _outb(0x3d4,14);
  _outb(0x3d5,((cursor_offset>>8)&0xFF));
  _outb(0x3d4,15);
  _outb(0x3d5,(cursor_offset&0xFF));
}

void putc(char c) {
  if(cursor_x>79) {
    cursor_x=0;
    cursor_y++;
  }
  if(cursor_y>24) {
    cursor_y=0;
    clear_screen();
  }
  switch(c) {					/* deal with a special char */
    case '\r': cursor_x=0; break;		/* carriage return */
    case '\n': cursor_x=0; cursor_y++; break; 	/* new ligne */	
    case 0x8 : if(cursor_x>0) cursor_x--; break;/* backspace */
    case 0x9 : cursor_x=(cursor_x+8)&~7; break;	/* tabulation */
						/* or print a simple character */
    default  : 
      video_memory[(cursor_x+80*cursor_y)*2]=c;
      cursor_x++;
      break;
  }
  setCursor();
}

void controller() {
  while(1) {
    if(_inb(0x64) == (0x1D)){
      _outb(0x64, 0x1C);
      putc(keyboard_map(_inb(0x60))); //Question 3
      //puthex(_inb(0x60)); // Question 2
    }
  }
}

char keyboard_map(unsigned char input) {
  switch(input) {
    case 0xB: return '0'; //0
    case 0x2: return '1'; //1
    case 0x3: return '2'; //2
    case 0x4: return '3'; //3
    case 0x5: return '4'; //4
    case 0x6: return '5'; //5
    case 0x7: return '6'; //6
    case 0x8: return '7'; //7
    case 0x9: return '8'; //8
    case 0xA: return '9'; //9
    case 0x10: return 'a'; //a
    case 0x11: return 'z'; //z
    case 0x12: return 'e'; //e
    case 0x13: return 'r'; //r
    case 0x14: return 't'; //t
    case 0x15: return 'y'; //y
    case 0x16: return 'u'; //u
    case 0x17: return 'i'; //i
    case 0x18: return 'o'; //o
    case 0x19: return 'p'; //p
    case 0x1E: return 'q'; //q
    case 0x1F: return 's'; //s
    case 0x20: return 'd'; //d
    case 0x21: return 'f'; //f
    case 0x22: return 'g'; //g
    case 0x23: return 'h'; //h
    case 0x24: return 'j'; //j
    case 0x25: return 'k'; //k
    case 0x26: return 'l'; //l
    case 0x27: return 'm'; //m
    case 0x2C: return 'w'; //w
    case 0x2D: return 'x'; //x
    case 0x2E: return 'c'; //c
    case 0x2F: return 'v'; //v
    case 0x30: return 'b'; //b
    case 0x31: return 'n'; //n
    default: return '\0';
  }
}

void funct_time() {
    int pos_x;
    int pos_y;
    int count = 0;

    while(1) {
        __asm volatile("cli");
        pos_x=cursor_x;
        pos_y=cursor_y;

        cursor_x = 70;
        cursor_y = 0;

        puts(itoa(count++, 10));

        cursor_x = pos_x;
        cursor_y = pos_y;
        __asm volatile("sti");
        _outb(0xA0, 0x20); 
        _outb(0x20, 0x20);
    }
}

char* itoa(int val, int base){
    static char buf[32] = {0};
    int i = 30;
    for(; val && i ; --i, val /= base)
        buf[i] = "0123456789abcdef"[val % base];

    return &buf[i+1];
}